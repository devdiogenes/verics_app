import IconCheckbox from "./Inputs/IconCheckbox.vue";
import Badge from "./Badge";

import DropDown from "./Dropdown.vue";
// Modals
import Modal from "./Modal.vue";

//Components
import PricingCard from "./Cards/PricingCard.vue";
import PricingComponent from "./Cards/PricingComponent.vue";

import RegisterCard from "./Cards/RegisterCard.vue";
import LoginCard from "./Cards/LoginCard.vue";
import ResetCard from "./Cards/ResetCard.vue";
import DashboardCard from "./Cards/DashboardCard.vue";
import BfCookieCard from "./Cards/BFCookieCard.vue";
import UserCard from "./Cards/UserCard.vue";
import EditProfileForm from "./Forms/EditProfileForm.vue";
import Collapse from "./Collapse.vue";
import Slider from "./Slider.vue";
import SimpleWizard from "./Wizard/Wizard.vue";
import WizardTab from "./Wizard/WizardTab.vue";
import Pagination from "./Pagination.vue";
import SidebarPlugin from "./SidebarPlugin";
import AnimatedNumber from "./AnimatedNumber";
import ContactForm from "./Forms/ContactForm.vue";
import StatsCard from "./Cards/StatsCard.vue";
import ChartCard from "./Cards/ChartCard.vue";
import GlobalSalesCard from "./Cards/GlobalSalesCard.vue";

//GRAPHS
import SGraphCheddar from "./Graphs/SGraphCheddar.vue";
import SGraphClouds from "./Graphs/SGraphClouds.vue";
import SGraphEqualizer from "./Graphs/SGraphEqualizer.vue";
import SGraphMap from "./Graphs/SGraphMap.vue";
import SGraphSnake from "./Graphs/SGraphSnake.vue";

import Tabs from "./Tabs.vue";
import Leakages from "./Leakages.vue";
import Documents from "./Documents.vue";
import Domains from "./Domains.vue";

export {
  GlobalSalesCard,
  ChartCard,
  StatsCard,
  IconCheckbox,
  Badge,
  Modal,
  Pagination,
  PricingCard,
  RegisterCard,
  LoginCard,
  DropDown,
  SidebarPlugin,
  Tabs,
  Slider,
  SimpleWizard,
  WizardTab,
  AnimatedNumber,
  Collapse,
  DashboardCard,
  ResetCard,
  BfCookieCard,
  ContactForm,
  EditProfileForm,
  UserCard,
  PricingComponent,
  Leakages,
  Domains,
  Documents,
  SGraphMap,
  SGraphCheddar,
  SGraphClouds,
  SGraphEqualizer,
  SGraphSnake
};
