import BF_APP_COMMON from "./BF_APP_COMMON";
// TODO: Para futureas versiones, esta librería debería extender de BFS e instanciar sólo esta en MAIN.js.
//      De esta manera, todo el código se puede referir a "$APP_SERVICES" sin distinguir entre BFS o APPS
export default class APPS extends BF_APP_COMMON {
  constructor(router) {
    super(router);
  }

  PGetDetailsUser() {
    return this._call_service("/PGetDetailsUser");
  }

  VDSnake() {
    this.#Add_token_report();
    return this._call_service("VDSnake");
  }

  VDMap() {
    this.#Add_token_report();
    return this._call_service("VDMap");
  }

  VDCheddar() {
    this.#Add_token_report();
    return this._call_service("VDCheddar");
  }

  VDTable() {
    this.#Add_token_report();
    return this._call_service("VDTable");
  }

  VRSummary() {
    this.#Add_token_report();
    return this._call_service("VRSummary");
  }

  VRLeakages_Equalizer() {
    this.#Add_token_report();
    return this._call_service("/VRLeakages_Equalizer");
  }

  VRLeakages_Cheddar() {
    this.#Add_token_report();
    return this._call_service("/VRLeakages_Cheddar");
  }

  VRLeakages_Table() {
    this.#Add_token_report();
    return this._call_service("/VRLeakages_Table");
  }

  VRDocuments_Equalizer() {
    this.#Add_token_report();
    return this._call_service("/VRDocuments_Equalizer");
  }

  VRDocuments_Cloud() {
    this.#Add_token_report();
    return this._call_service("/VRDocuments_Cloud");
  }

  VRDocuments_Table() {
    this.#Add_token_report();
    return this._call_service("/VRDocuments_Table");
  }

  VRDomains_Equalizer() {
    this.#Add_token_report();
    return this._call_service("/VRDomains_Equalizer");
  }

  VRDomains_Map() {
    this.#Add_token_report();
    return this._call_service("/VRDomains_Map");
  }

  VRDomains_Table() {
    this.#Add_token_report();
    return this._call_service("/VRDomains_Table");
  }

  VRRaw_Table() {
    this.#Add_token_report();
    return this._call_service("/VRRaw_Table");
  }

  #Add_token_report() {
    this.data["token_report"] =
      this.router.currentRoute.query.token != null
        ? this.router.currentRoute.query.token
        : localStorage.getItem("op_long_token");
  }
}
