export default {
  APP_NAME: "DASHBOARD",
  APP_ADD_SITE_BUTTON_LABEL: "AÑADIR COMPAÑÍA",
  ///////////////////////////////////////////////////
  APP_PRICING_NAME: "Planes",
  APP_PRICING_TITLE: "Planes que se adaptan a ti",
  APP_PRICING_SUBTITLE:
    "Todas las opciones de disponibles para empresas y particulares",

  APP_PRICING_CARD_1_TITLE: "Freelancer",
  APP_PRICING_CARD_1_ICON: "free_breakfast",
  APP_PRICING_CARD_1_PRICE: "FREE",
  APP_PRICING_CARD_1_BODY: "FRELANCER descripcion ESPAÑOL",
  APP_PRICING_CARD_1_BUTTON: "TEXTO DEL BOTON",

  APP_PRICING_CARD_2_TITLE: "EMPRESA_PEQUEÑA",
  APP_PRICING_CARD_2_ICON: "workspace_premium",
  APP_PRICING_CARD_2_PRICE: "€",
  APP_PRICING_CARD_2_BODY: "EMPRESA_PEQUEÑA descripcion ESPAÑOL",
  APP_PRICING_CARD_2_BUTTON: "TEXTO DEL BOTON",

  APP_PRICING_CARD_3_TITLE: "EMPRESA_MEDIANA",
  APP_PRICING_CARD_3_ICON: "business",
  APP_PRICING_CARD_3_PRICE: "€€",
  APP_PRICING_CARD_3_BODY: "EMPRESA_MEDIANA descripcion ESPAÑOL",
  APP_PRICING_CARD_3_BUTTON: "TEXTO DEL BOTON",

  APP_PRICING_CARD_4_TITLE: "CUSTOM",
  APP_PRICING_CARD_4_ICON: "auto_fix_high",
  APP_PRICING_CARD_4_PRICE: "Contacta con nosotros",
  APP_PRICING_CARD_4_BODY: "CUSTOM descripcion ESPAÑOL",
  APP_PRICING_CARD_4_BUTTON: "TEXTO DEL BOTON",

  ////////////////////////////////////////////////////////

  APP_GRAPH_CLOUDS_LEGEND_SENSIBLE: "Sensible",
  APP_GRAPH_CLOUDS_LEGEND_LEAKAGES: "Filtración",

  APP_GRAPH_EQUALIZER_MULTIPLE_LEGEND_SENSIBLE: "Sensible",
  APP_GRAPH_EQUALIZER_MULTIPLE_LEGEND_LEAKAGES: "Filtración",

  APP_GRAPH_MAPH_TITLE: "Ubicación",

  APP_GRAPH_CHEDDAR_LEGEND_SENSIBLE: "Sensible",
  APP_GRAPH_CHEDDAR_LEGEND_LEAKAGES: "Filtración",

  APP_DAHSBOARD_NOTIFICATION_TEXT_NO_ACTIVITY: "¡Aún no tienes actividad!",
  APP_DAHSBOARD_NOTIFICATION_TEXT_ADD_COMPANIES:
    "Añade compañías para empezar.",

  APP_S_INFO_TABLE_TITLE: "Mis compañías",
  APP_S_INFO_TABLE_FILTER: "Filtrar por estado",
  APP_S_INFO_TABLE_DOWNLOAD_INFO: "Infomación completa para descargar.",
  APP_S_INFO_TABLE_COLUMN_TITLE_WEB: "Web",
  APP_S_INFO_TABLE_COLUMN_TITLE_COMPANY: "Compañía",
  APP_S_INFO_TABLE_COLUMN_TITLE_DOMAIN: "Dominio",
  APP_S_INFO_TABLE_COLUMN_TITLE_APLICATION_DATE: "Solicitud",
  APP_S_INFO_TABLE_COLUMN_TITLE_STATE: "Estado",
  APP_S_INFO_TABLE_COLUMN_TITLE_METADATA: "Metadatos",
  APP_S_INFO_TABLE_COLUMN_TITLE_RGPD: "RGPD",

  APP_S_INFO_TABLE_STATES_ANALYZE: "Analizar",
  APP_S_INFO_TABLE_STATES_FINISHED: "Terminado",
  APP_S_INFO_TABLE_STATES_ANALYZING: "Analizando",
  APP_S_INFO_TABLE_STATES_WORKING: "Trabajando",
  APP_S_INFO_TABLE_STATES_QUEUED: "En cola",
  APP_S_INFO_TABLE_STATES_WITHOUT_ANALYZING: "Sin analizar",

  APP_S_INFO_TABLE_STATES_FILTER_ALL: "Todos",
  APP_S_INFO_ACTIONS_VIEW_REPORT: "Ver informe",
  APP_S_INFO_ACTIONS_CLEANING: "Limpiar metadatos",
  APP_S_INFO_ACTIONS_RGPD_ANALYSE: "Verificar",
  APP_S_INFO_ACTIONS_CLEANING_DOWNLOAD_ZIP: "Descargar archivos limpios",

  APP_S_INFO_ACTIONS_CLEANING_TITLE_MODAL:
    "¿Quieres limpiar los metadatos del dominio:",
  APP_S_INFO_ACTIONS_CLEANING_TEXT_MODAL:
    "Esto puede conllevar gastos adicionales",
  APP_S_INFO_ACTIONS_CLEANING_TITLE_YES: "Si, Limpialo",

  APP_S_INFO_CLEANING_ENDED_TITLE: "Caducó el: ",
  APP_S_INFO_CLEANING_AVAILABLE_UNTIL_TITLE: "Disponible hasta: ",
  APP_S_INFO_CLEANING_TOO_MANY_FILES_TITLE: "Contacte a: sos@suments.com",

  APP_S_INFO_TABLE_REPORT_LEAKAGES_NUM_OF_APPEARANCES: "Nº de Apariciones",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_DATA: "Dato",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_BAN: "Banear",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_BAN_TEXT: "Ocultar este dato de mi tabla",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_DIRECT_SEARCH: "Búsqueda directa",

  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_TITLE:
    "¿Estás seguro de que quieres banear",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_ALERT:
    "¡Esta acción no se puede deshacer!",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_YES: "Sí, nada me haría más feliz",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_NO: "No",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_ERRASING:
    "Estamos borrando todas las apariciones",

  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_FINISHED: "¡Terminado!",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_ELIMINATED: "Hemos eliminado",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_LEAKS_WITH_DATA: "filtraciones con el dato",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_SOMETHING_WENT_WRONG: "Ups! Algo salio mal",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_TRY_LATER: "Vuelvelo a intentar más tarde",

  APP_S_DOCUMENTS_TABLE_COLUMN_FACT: "Dato",
  APP_S_DOCUMENTS_TABLE_COLUMN_DOCUMENT: "Documento",
  APP_S_DOCUMENTS_TABLE_COLUMN_DOMAIN: "Dominio",
  APP_S_DOCUMENTS_TABLE_COLUMN_FILE_TYPE: "Tipo de archivo",

  APP_S_DOMAINS_TABLE_COLUMN_FACT: "Dato",
  APP_S_DOMAINS_TABLE_COLUMN_DOMAIN: "Dominio",
  APP_S_DOMAINS_TABLE_COLUMN_RISKS: "Riesgos",

  APP_REPORT_S_RAW_TABLE_COLUMN_SEVERITY: "Severidad de la filtración",
  APP_REPORT_S_RAW_TABLE_COLUMN_DATA: "Dato filtrado",
  APP_REPORT_S_RAW_TABLE_COLUMN_DOMAIN: "Dominio",
  APP_REPORT_S_RAW_TABLE_COLUMN_LINK: "Enlace al archivo",
  APP_REPORT_S_RAW_TABLE_COLUMN_FILE_TYPE: "Tipo de archivo",

  /////////////////////////////////////////////////////////////
  APP_REPORT_S_SUMMARY_BUTON_COPY_URL: "Copiar url del informe",
  APP_REPORT_S_SUMMARY_BUTON_COPY_URL_HIDE: "Informe anonimizado",
  APP_REPORT_S_SUMMARY_ANALYSIS_TITLE: "Análisis de metadatos",
  APP_REPORT_S_SUMMARY_TITLE: "Resumen",
  APP_REPORT_S_REPORT: "Filtraciones encontradas",
  APP_REPORT_S_SUMMARY_LEAKSFOUND_TITLE: "Filtraciones encontradas",
  APP_REPORT_S_SUMMARY_LEAKSNOTFOUND_TITLE:
    "No se han encontrado filtraciones para este dominio",
  APP_REPORT_S_SUMMARY_LEAKSFOUND_SUBTITLE:
    "Se han encontrado filtraciones de datos",
  APP_REPORT_S_SUMMARY_LEAKSFOUND_BUTTON: "Ver filtraciones",
  APP_REPORT_S_SUMMARY_DOCUMENTS_TITLE: "Documentos analizados",
  APP_REPORT_S_SUMMARY_DOCUMENTS_SUBTITLE: "Analizado con Verics",
  APP_REPORT_S_SUMMARY_METAWASH_TITLE: "Metawash",
  APP_REPORT_S_SUMMARY_METAWASH_SUBTITLE:
    "Limpia las filtraciones encontradas con Metawash",
  APP_REPORT_S_SUMMARY_METAWASH_BUTTON: "Limpiar filtraciones",
  APP_REPORT_S_SUMMARY_METAWASH_DOWNLOAD: "Descargar archivos",
  APP_REPORT_S_SUMMARY_METAWASH_TOO_MANY_FILES: "Demasiados archivos",
  APP_REPORT_S_SUMMARY_METAWASH_AVAILABLE_TITLE: "Disponible hasta:",
  APP_REPORT_S_SUMMARY_METAWASH_STATE_TITLE: "Estado metawash:",
  APP_REPORT_S_SUMMARY_METAWASH_EXPIRED_TITLE: "Caducó el:",

  APP_REPORT_S_SUMMARY_RGPD_TITLE: "Cumplimiento RGPD",
  APP_REPORT_S_SUMMARY_RGPD_SUBTITLE:
    "Comprueba si la web cumple con la normativa de protección de datos",
  APP_REPORT_S_SUMMARY_RGPD_BUTTON: "Comprobar RGPD",
  APP_REPORT_S_REPORT_TITLE: "Informe completo",
  APP_REPORT_S_LEAKAGES_TITLE: "Potenciales filtraciones",
  APP_REPORT_S_LEAKAGES_SUBTITLE:
    "En esta sección se muestran todas las filtraciones encontradas del dominio analizado. Podrás obtener una visión global del volumen de las filtraciones y cada dato encontrado.",
  APP_REPORT_S_SUBTITLE_SEE_MORE: "Saber más",
  APP_REPORT_S_LEAKAGES_TABLE_SUBTITLE:
    "Aquí puedes ver en detalle los datos encontrados, el número de apariciones y hacer una búsqueda rápida en las redes.",
  APP_REPORT_S_LEAKAGES_EQUALIZER_TITLE: "Potenciales filtraciones",
  APP_REPORT_S_LEAKAGES_CHEDDAR_TITLE: "Potenciales filtraciones",
  APP_REPORT_S_LEAKAGES_TABLE_FILTER_TITLE: "Filtrar por severidad",
  APP_REPORT_S_LEAKAGES_TABLE_FILTER_OPTION_ALL: "Todos",

  APP_REPORT_S_DOCUMENTS_TITLE: "Documentos",
  APP_REPORT_S_DOCUMENTS_SUBTITLE:
    "Aquí puedes ver las filtraciones que hemos encontrado según el tipo de archivo en el que se encuentran. Así podrás lidentificar los canales de filtración más afectados.",
  APP_REPORT_S_DOCUMENTS_TABLE_SUBTITLE:
    "En esta tabla puedes ver en qué archivo se encuentra cada filtración. Puedes acceder al archivo en un sólo clic.",
  APP_REPORT_S_DOCUMENTS_EQUALIZER_TITLE:
    "Así es como se distribuyen las filtraciones de tus datos",
  APP_REPORT_S_DOCUMENTS_CLOUDS_TITLE: "Distrubución de los tipos de archivo",
  APP_REPORT_S_DOCUMENTS_TABLE_FILTER_TITLE: "Filtrar por tipo de documento",

  APP_REPORT_S_DOMAINS_TITLE: "Dominio y subdominios",
  APP_REPORT_S_DOMAINS_SUBTITLE:
    "En ocasiones los dominios tienen subdominios. Aquí puedes observar cómo se distribuyen las filtraciones según los diferentes subdominios encontrados. También puedes echar un ojo a la distribución geográfica de los servidores en los que se hayan.",
  APP_REPORT_S_DOMAINS_TABLE_SUBTITLE:
    "Aquí puedes ver claramente en qué subdominio se encuentra cada filtración.",
  APP_REPORT_S_DOMAINS_EQUALIZER_TITLE:
    "Así es la distribución de filtraciones agrupadas por dominios",
  APP_REPORT_S_DOMAINS_MAP_TITLE: "Ubicación de los dominios encontrados",
  APP_REPORT_S_DOMAINS_TABLE_FILTER_TITLE: "Filtrar por severidad",

  BF_FOOTER_DEVELOP_TITLE: "Tecnología desarrollada por",

  /////////////////////////////////////////////////////////////////
  APP_ADD_DOMAIN_FORM_TITLE: "Añadir compañía",
  APP_ADD_DOMAIN_FORM_SUBTITLE: "Introduce aquí los datos",
  APP_ADD_DOMAIN_FORM_COMPANY_NAME: "*Nombre de la empresa",
  APP_ADD_DOMAIN_FORM_COMPANY_CONTACT_NAME: "Contacto de la empresa",
  APP_ADD_DOMAIN_FORM_COMPANY_EMAIL: "Email de contacto",
  APP_ADD_DOMAIN_FORM_COMPANY_TELEPHONE: "Teléfono de contacto",
  APP_ADD_DOMAIN_FORM_COMPANY_WEBSITE: "*Web (ej.: www.suments.com)",

  APP_COPY_NOTIFICATION_MESSAGE: "Enlace copiado al portapapeles",

  APP_RGPD_POPUP_TEXT:
    "Estamos desarrollando esta nueva funcionalidad, si estás interesado escríbenos a ",
  APP_RGPD_POPUP_TITLE: "Cumplimiento del RGPD ",

  //////////////////////////////////////////////////////////////
  APP_ADD_COMPANY_ERROR_NAME_EMPTY:
    "Por favor, introduce el nombre de la compañía",
  APP_ADD_COMPANY_ERROR_DOMAIN_EMPTY:
    "Por favor, introduce el dominio a analizar",
  APP_ADD_COMPANY_ERROR_INVALID_EMPTY:
    "Este nombre de dominio no es válido. Puedes ver ejemplos de nombres de dominios válidos ",
  APP_ADD_COMPANY_ERROR_INVALID_EMPTY_LINK: "aquí.",
  APP_ADD_COMPANY_BUTTON: "Añadir compañía",
  APP_BF_ADD_DOMAIN_TEXT_COPIED: "Texto copiado al portapapeles",
  APP_BF_ADD_DOMAIN_ERROR_COPY_TEXT:
    "No se pudo copiar el texto al portapapeles"
};
