export default {
  BF_DASHBOARD_NAME: "DASHBOARD",
  BF_DASHBOARD_TITTLE: "DASHBOARD",
  BF_DASHABOARD_NOTIFICATION_BUTTON_LABEL: "Contratar",
  BF_DASHABOARD_NOTIFICATION_LABEL: "You're using the free version,",
  BF_DASHABOARD_NOTIFICATION_BUTTON_LABEL: "Contract",

  BF_DASHBOARD_SUMMARY_CARD1_TITTLE: "CARD 1.Title",
  BF_DASHBOARD_SUMMARY_CARD1_BODY: "CARD1_SUMMARY_BODY",
  BF_DASHBOARD_SUMMARY_CARD1_FOOTER: "CARD1_FOOTER",

  BF_DASHBOARD_SUMMARY_CARD2_TITTLE: "CARD2.Title",
  BF_DASHBOARD_SUMMARY_CARD2_BODY: "CARD2_SUMMARY_BODY",
  BF_DASHBOARD_SUMMARY_CARD2_FOOTER: "CARD2_FOOTER",

  BF_DASHBOARD_SUMMARY_CARD3_TITTLE: "CARD 3.Title",
  BF_DASHBOARD_SUMMARY_CARD3_BODY: "CARD3_SUMMARY_BODY",
  BF_DASHBOARD_SUMMARY_CARD3_FOOTER: "CARD3_FOOTER",

  BF_DASHBOARD_SUMMARY_CARD4_TITTLE: "CARD 1.Title",
  BF_DASHBOARD_SUMMARY_CARD4_BODY: "CARD4_SUMMARY_BODY",
  BF_DASHBOARD_SUMMARY_CARD4_FOOTER: "CARD4_FOOTER",

  BF_DASHBOARD_SUMMARY_GRAPHS_CARD1_TITTLE: "CARD 1.Title",
  BF_DASHBOARD_SUMMARY_GRAPHS_CARD1_BODY: "CARD4_SUMMARY_GRAPH_BODY",
  BF_DASHBOARD_SUMMARY_GRAPHS_CARD1_FOOTER: "CARD1_FOOTER",

  BF_DASHBOARD_SUMMARY_GRAPHS_CARD2_TITTLE: "CARD 1.Title",
  BF_DASHBOARD_SUMMARY_GRAPHS_CARD2_BODY: "CARD2_SUMMARY_GRAPH_BODY",
  BF_DASHBOARD_SUMMARY_GRAPHS_CARD2_FOOTER: "CARD2_FOOTER",

  BF_DASHBOARD_SUMMARY_GRAPHS_CARD3_TITTLE: "CARD 3.Title",
  BF_DASHBOARD_SUMMARY_GRAPHS_CARD3_BODY: "CARD3_SUMMARY_GRAPHBODY",
  BF_DASHBOARD_SUMMARY_GRAPHS_CARD3_FOOTER: "CARD3_FOOTER",
  //
  BF_DASHBOARD_SUMMARY_ADD_SITE_TITTLE: "ADD SITE",
  BF_DASHBOARD_SUMMARY_ADD_SITE_BODY: "You can add new site here.",
  BF_DASHBOARD_SUMMARY_ADD_SITE_FOOTER:
    "If you have a question, please contact the administrator",

  BF_DASHBOARD_SUMMARY_MY_SITE_TITTLE: "MY SITE",
  BF_DASHBOARD_SUMMARY_MY_SITE_BODY: "You can see my site infomation.",
  BF_DASHBOARD_SUMMARY_MY_SITE_FOOTER:
    "If you have anything to ask please contact the administrator",
  ////////////////////////////////////////////////////
  BF_PASS_RESET_TITLE: "Set your new password",
  BF_PASS_RESET_BUTTON: "Go!",
  BF_PASS_RESET_INPUT: "Your new password here...",
  BF_PASS_RESET_ERROR_EMPTY_PASS: "Please type one password",
  BF_PASS_RESET_ERROR_GENERIC: "Something went wrong. Try again",
  ////////////////////////////////////////////////////
  BF_LOGIN_NAME: "Login",
  BF_LOGIN_FORM_TITLE: "Login",
  BF_LOGIN_FORM_SUBTITLE: "",
  BF_LOGIN_FORM_EMAIL: "Your email...",
  BF_LOGIN_FORM_PASSWORD: "Your pass...",
  BF_LOGIN_FORM_SUBMIT: "Let's go!",
  BF_LOGIN_REGISTER_LABEL: "New in the house?",
  BF_LOGIN_REGISTER_LINK: "Register here!",
  BF_LOGIN_RESET_LINK: "Forgot your password?",
  ///////////////////////////////////////////////////
  BF_PRICING_NAME: "Plans",
  BF_PRICING_TITLE: "Choose the best plan for you",
  BF_PRICING_SUBTITLE: "Ble ble ble ble ble",

  BF_PRICING_CARD_1_TITLE: "Freelancer",
  BF_PRICING_CARD_1_ICON: "free_breakfast",
  BF_PRICING_CARD_1_PRICE: "FREE",
  BF_PRICING_CARD_1_BODY: "FRELANCER description ENGLISH",
  BF_PRICING_CARD_1_BUTTON: "BUTTON TEXT",

  BF_PRICING_CARD_2_TITLE: "COMPANY_SMALL",
  BF_PRICING_CARD_2_ICON: "workspace_premium",
  BF_PRICING_CARD_2_PRICE: "€",
  BF_PRICING_CARD_2_BODY: "COMPANY_SMALL description ENGLISH",
  BF_PRICING_CARD_2_BUTTON: "BUTTON TEXT",

  BF_PRICING_CARD_3_TITLE: "COMPANY_MEDIUM",
  BF_PRICING_CARD_3_ICON: "business",
  BF_PRICING_CARD_3_PRICE: "€€",
  BF_PRICING_CARD_3_BODY: "COMPANY_MEDIUM description ENGLISH",
  BF_PRICING_CARD_3_BUTTON: "BUTTON TEXT",

  BF_PRICING_CARD_4_TITLE: "CUSTOM",
  BF_PRICING_CARD_4_ICON: "auto_fix_high",
  BF_PRICING_CARD_4_PRICE: "Contact us",
  BF_PRICING_CARD_4_BODY:
    "We will give you a customized quotation according to your needs",
  BF_PRICING_CARD_4_BUTTON: "Contact",
  ///////////////////////////////////////////////////
  BF_REGISTER_NAME: "Register",
  BF_REGISTER_TITLE: "Register",

  BF_REGISTER_LEFT_1_TITLE: "Analyzes and scans sensitive web metadata",
  BF_REGISTER_LEFT_1_BODY: "",
  BF_REGISTER_LEFT_2_TITLE:
    "In-depth analysis of the web and its millions of metadata",
  BF_REGISTER_LEFT_2_BODY: "",
  BF_REGISTER_LEFT_3_TITLE:
    "Study and audit to clean metadata with Metacleaner",
  BF_REGISTER_LEFT_3_BODY: "",

  BF_REGISTER_FORM_FIRST_NAME: "Your name...",
  BF_REGISTER_FORM_EMAIL: "Your email...",
  BF_REGISTER_FORM_PASSWORD: "Your pass...",
  BF_REGISTER_FORM_PASSWORD_CONFIRMATION: "Your pass again...",
  BF_REGISTER_FORM_ACCEPT:
    "I agree with the basic data protection information made available to me and with the ",
  BF_REGISTER_FORM_ACCEPT_2: "accepting both without reservation.",
  BF_REGISTER_FORM_ACCEPT_LINK_TITLE: "General Terms and Conditions",
  BF_REGISTER_FORM_CONSENT:
    "I give my consent to Suments Data Ltd. to receive electronic commercial communications of its services.",
  BF_REGISTER_FORM_CONSENT_LEGAL_SUBTITLE_1:
    "Responsible: Suments Data Ltd. Purpose: Allows the user to register on the website. Send electronic commercial communications of our services. Legitimation: Execution of a contract. Consent of the interested party. Recipients: No data will be transferred to third parties, unless legally required. Rights: You have the right to access, rectify and delete your data, as well as other rights, indicated in the additional information, which can be exercised at",
  BF_REGISTER_FORM_CONSENT_LEGAL_SUBTITLE_2:
    ". Additional information: Detailed information on Data Protection can be found at ",
  BF_REGISTER_FORM_TERMS_AND_CONDITIONS: "General terms and conditions",
  BF_REGISTER_FORM_ACCEPT: "I agree ",
  BF_REGISTER_FORM_TERMS_AND_CONDITIONS: "Terms and conditions",
  BF_REGISTER_FORM_SUBMIT: "Let's go!",
  BF_REGISTER_FORM_TERMS_AND_CONDITIONS_ACCEPT:
    "Please accept the terms and conditions",
  BF_REGISTER_ERROR: "Something went wrong... =(. Please try again.",
  BF_REGISTER_ERROR_MATCH_PASSW: "Ouch... The passwords don't match.",
  BF_REGISTER_FORM_INVALID_EMAIL: "Please enter a valid email address.",
  BF_REGISTER_FORM_ERROR_MAIL_ALREADY_REGISTER:
    "An account already exists with this email, login or create an account with a different email address.",
  BF_REGISTER_FORM_ERROR_NAME_EMPTY: "Please enter a name",
  BF_REGISTER_FORM_ERROR_PASSWORD_EMPTY: "Please enter a password",
  ////////////////////////////////////////////////////
  BF_RESET_FORM_TITLE: "Recover your password",
  BF_RESET_FORM_SUBTITLE: "Where do you want us to send the recovery email?",
  BF_RESET_FORM_SUBMIT: "Request",
  BF_BF_RESET_FORM_SUBTITLERESET_LOGIN_LABEL: "Back to login",
  BF_RESET_FORM_CONFIRMATION_SUBTITLE:
    "Email on its way!. If it does not arrive in couple of moments, please request a new one. =)",
  BF_RESET_FORWARD_BUTTON: "Resend",
  BF_RESET_FORWARD_LABEL: "Back to login",
  BF_RESET_LOGIN_LABEL: "Back to login",
  BF_LOGIN_ERROR: "Something does not match. Please try again. =)",

  ////////////////////////////////////////////////////
  BF_PAYMENT_SUCCESS_TITLE: "Successful payment!",
  BF_PAYMENT_SUCCESS_SUBTITLE: "You will soon receive a confirmation email",
  BF_PAYMENT_SUCCESS_BUTTON: "Back to Verics",

  ////////////////////////////////////////////////////

  BF_PAYMENT_FAIL_TITLE: "Ups..... something went wrong!",
  BF_PAYMENT_FAIL_SUBTITLE: "Email us at hola@suments.com",
  BF_PAYMENT_FAIL_BUTTON: "Back to Verics",

  //////////////////////////////////////////////////

  BF_ERROR_PAGE_DOMAIN_TITLE: "Ups..... something went wrong!",
  BF_ERROR_PAGE_DOMAIN_SUBTITLE: "Domain not found",
  BF_ERROR_PAGE_DOMAIN_BUTTON: "Back to Verics",

  //////////////////////////////////////////////////

  BF_ERROR_PAGE_TITLE: "Ups..... something went wrong!",
  BF_ERROR_PAGE_SUBTITLE: "Page not found",
  BF_ERROR_PAGE_BUTTON: "Back to Verics",

  //////////////////////////////////////////////////

  BF_USER_PROFILE_NAME: "User Page",
  BF_USER_PROFILE_TITLE: "Your details",
  BF_USER_PROFILE_SUBTITLE: "Change it here",

  BF_USER_PROFILE_FORM_COMPANY: "Company",
  BF_USER_PROFILE_FORM_NAME_USER: "@Username",
  BF_USER_PROFILE_FORM_EMAIL: "Email",
  BF_USER_PROFILE_FORM_NAME_FIRST: "First name",
  BF_USER_PROFILE_FORM_NAME_LAST: "Last name",
  BF_USER_PROFILE_FORM_ADDRESS: "Address",
  BF_USER_PROFILE_FORM_CITY: "City",
  BF_USER_PROFILE_FORM_PHONE: "Phone",
  BF_USER_PROFILE_FORM_WEB: "Website",
  BF_USER_PROFILE_FORM_SUBMIT: "Update my info",
  BF_USER_PROFILE_MEMBER_SINCE: "Member since",
  BF_USER_PROFILE_LANGUAGE: "Language",
  BF_USER_PROFILE_SUBMIT_ERROR:
    "Ouch... Something went wrong. Please try again. =)",
  /////////////////////////////////////////////////////

  BF_CONTACT_US_FORM_ISSUE: "Issue",
  BF_CONTACT_US_FORM_MESSAGE: "Write your query here",
  /////////////////////////////////////////////////////

  BF_MENU_APP_NAME: "MY_APP_ENGLISH",
  ////////////////////////////////////////////////////
  BF_LANDING_BRIEF_TITLE: "Analyze and scan for metadata leaks on your website",

  BF_LANDING_BRIEF_BODY:
    "Verics® analyzes the metadata of web pages that contain a large amount of personal information and involve a violation of the GDPR. Once the analysis is complete, Verics® summarizes all data breaches found in a report, organized by file type, severity and subdomains found.",
  BF_LANDING_BRIEF_BUTTON_LABEL: "Start using it!",

  BF_LANDING_FORM_TITLE: "Any questions about how Verics works?",
  BF_LANDING_FORM_SUBTITLE: "SUBTITLE CONTACT FORM",
  BF_LANDING_FORM_LEGAL:
    "Basic information on data processing in accordance with RGPD (EU) 2016/679 and LOPDGDD 3/2018. Responsible: Suments Data Ltd. Purpose: Contact the interested party. Send commercial communications about our services by letter, telephone, email, SMS / MMS, WhatsApp, Telegram or by other equivalent electronic means of communication, as long as the interested party has consented to the processing of their personal data for this purpose. Legitimation: Consent of the interested party. Recipients: No data will be transferred to third parties, except legal obligation. Rights: You have the right to access, rectify and delete your data, as well as other rights, indicated in the additional information, which you can exercise by contacting the address of the person responsible for the treatment at hola@suments.com Additional information: You can consult the additional information and Detailed information on Data Protection in the attached clauses found at https://www.Verics.com/politica-de-privacidad",
  BF_LANDING_FORM_CONSENT_LEGAL:
    "I accept the processing of my personal data for the purposes indicated in the basic data protection information made available to me.",
  BF_LANDING_FORM_LEGAL:
    "Información básica sobre el tratamiento de datos personales conforme al RGPD (UE) 2016/679 y a la LOPDGDD 3/2018.    Responsable: Suments Data Ltd. Finalidad: Contactar con el interesado. Enviar comunicaciones comerciales electrónicas de nuestros servicios. Legitimación: Consentimiento del interesado. Destinatarios: No se cederán datos a terceros, salvo obligación legal. Derechos: Tiene derecho a acceder, rectificar y suprimir sus datos, así como otros derechos, indicados en la información adicional, que puede ejercer en hola@suments.com Información adicional: Puede consultar la información detallada sobre Protección de Datos en https://Verics.com/#/politica-de-privacidad ",
  BF_LANDING_FORM_CONSENT_LEGAL:
    "I agree to the processing of my data according to the purposes indicated in the basic data protection information.",
  BF_LANDING_FORM_CONSENT_LEGAL_SUBTITLE_1:
    "Responsible: Suments Data Ltd. Purpose: To contact the person concerned. Send electronic commercial communications of our services. Legitimation: Consent of the interested party. Recipients: No data will be transferred to third parties, unless legally required. Rights: You have the right to access, rectify and delete your data, as well as other rights, indicated in the additional information, which can be exercised at ",
  BF_LANDING_FORM_CONSENT_LEGAL_SUBTITLE_2:
    ". Additional information: Detailed information on Data Protection can be found at ",
  BF_LANDING_FORM_CONSENT_LEGAL_TITLE:
    "*Basic information on the processing of personal data in accordance with RGPD (EU) 2016/679 and LOPDGDD 3/2018.",
  BF_LANDING_FORM_CONSENT_LEGAL_SUBTITLE_1:
    "Responsible: Suments Data Ltd. Purpose: Allows the user to register on the website. Send electronic commercial communications of our services. Legitimation: Execution of a contract. Consent of the interested party. Recipients: No data will be transferred to third parties, unless legally required. Rights: You have the right to access, rectify and delete your data, as well as other rights, indicated in the additional information, which can be exercised at",
  BF_LANDING_FORM_CONSENT_LEGAL_SUBTITLE_2:
    ". Additional information: You can consult the detailed information on Data Protection in ",
  BF_LANDING_FORM_CONSENT_COMMERCIAL:
    "I give my consent to Suments Data Ltd to receive electronic commercial communications of its services.",
  BF_LANDING_FORM_SUBMIT: "Send to",
  BF_LANDING_FORM_SUBMIT_ERROR:
    "Oops... Something went wrong. Please try it again. =D",
  BF_LANDING_FORM_MISSING_VALUES: "Please fill in all required fields *.",
  BF_LANDING_FORM_CONSENT_LEGAL_WEB_LINK:
    "https://Verics.com/#/politica-de-privacidad",
  BF_LANDING_FORM_CONSENT_CGC_WEB_LINK:
    "https://www.Verics.com/condiciones-generales-contratacion",
  BF_LANDING_FORM_CONSENT_COMMERCIAL:
    "I authorize Suments Data Ltd to receive electronic commercial communications for its services.",
  BF_LANDING_FORM_SUBMIT: "Send",
  BF_LANDING_FORM_SUBMIT_ERROR:
    "Ouch... Something went wrong. Please try again. =)",
  BF_LANDING_FORM_MISSING_VALUES: "Please fill the required fields",
  BF_LANDING_FORM_CONSENT_LEGAL_WEB_LINK: "Terms and conditions",
  ////////////////////////////////////////////////////
  BF_LEFT_MENU_TITLE_1: "Dashboard",
  BF_LEFT_MENU_TITLE_2: "My Profile",
  BF_LEFT_MENU_TITLE_3: "Add Site",
  BF_LEFT_MENU_TITLE_4: "Logout",
  BF_LEFT_MENU_TITLE_5: "Support",

  ///////////////////////////////////////////////////
  BF_FOOTER_CONTACT_US: "Contact us",
  BF_FOOTER_TERMS_AND_CONDITIONS: "Terms and conditions",
  BF_FOOTER_NOTICE_PRIVACY: "privacy notice",
  BF_FOOTER_NOTICE_COOKIES: "Cookies policy",
  BF_FOOTER_NOTICE_LEGAL: "legal notice",
  BF_FOOTER_COPYRIGHT: "SUMENTS DATA",
  BF_FOOTER_COPYRIGHT_LINK: "https://suments.com/es/",
  BF_FOOTER_COPYRIGHT_SLOGAN: "We speak data!",

  /////////////////////////////////////////////////////
  BF_RESET_SUBMIT_ERROR: "Ouch... Something went wrong. Please try again. =)",
  BF_RESET_EMAIL_ERROR:
    "Please tell us an email... Otherwise we do not know where to send it...",
  BF_TOPBAR_: "",
  /////////////////////////////////////////////////////
  BF_COOKIE_MODAL_CONSENT_TITLE: "I use cookies",
  BF_COOKIE_MODAL_CONSENT_DESCRIPTION: "Your cookie consent message here",
  BF_COOKIE_MODAL_CONSENT_BUTTON_PRIMARY: "Accept",
  BF_COOKIE_MODAL_CONSENT_BUTTON_SECONDARY: "Reject",
  BF_COOKIE_MODAL_SETTINGS_TITLE: "Cookie settings",
  BF_COOKIE_MODAL_SETTINGS_SAVE: "Save settings",
  BF_COOKIE_MODAL_SETTINGS_ACCEPT_ALL: "Accept all",
  BF_COOKIE_MODAL_SETTINGS_CLOSE_ALL: "Close",
  BF_COOKIE_MODAL_SETTINGS_BLOCK_DISCLAIMER_TITLE: "Cookie usage",
  BF_COOKIE_MODAL_SETTINGS_BLOCK_DISCLAIMER_DESCRIPTION:
    "Your cookie usage disclaimer",
  BF_COOKIE_MODAL_SETTINGS_BLOCK_NECESSARY_TITLE: "Strictly necessary cookies",
  BF_COOKIE_MODAL_SETTINGS_BLOCK_NECESSARY_DESCRIPTION:
    "Category description ... ",
  BF_COOKIE_MODAL_SETTINGS_BLOCK_ANALYTICS_TITLE: "Analytics cookies",
  BF_COOKIE_MODAL_SETTINGS_BLOCK_ANALYTICS_DESCRIPTION:
    "Category description ...",

  BF_TABLE_SEARCH_LABEL: "Search",
  BF_TABLE_PAGINATION_LABEL: "Per page",
  BF_TABLE_PAGINATION_SHOWING_LABEL: "Showing of the",
  BF_TABLE_PAGINATION_FROM_LABEL: "from",
  BF_TABLE_PAGINATION_TO_LABEL: "at",
  BF_TABLE_PAGINATION_OF_LABEL: "of a total of",
  BF_TABLE_PAGINATION_ENTRIES_LABEL: "entries"
};
