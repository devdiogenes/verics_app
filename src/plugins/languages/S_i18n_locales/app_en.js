export default {
  APP_NAME: "DASHBOARD",
  APP_ADD_SITE_BUTTON_LABEL: "ADD COMPANY",
  ///////////////////////////////////////////////////
  APP_PRICING_NAME: "Plans",
  APP_PRICING_TITLE: "Plans that fit you",
  APP_PRICING_SUBTITLE: "All options available for companies and individuals",

  APP_PRICING_CARD_1_TITLE: "Freelancer",
  APP_PRICING_CARD_1_ICON: "free_breakfast",
  APP_PRICING_CARD_1_PRICE: "FREE",
  APP_PRICING_CARD_1_BODY: "FRELANCER descripcion ESPAÑOL",
  APP_PRICING_CARD_1_BUTTON: "TEXTO DEL BOTON",

  APP_PRICING_CARD_2_TITLE: "Small Company",
  APP_PRICING_CARD_2_ICON: "workspace_premium",
  APP_PRICING_CARD_2_PRICE: "€",
  APP_PRICING_CARD_2_BODY: "EMPRESA_PEQUEÑA descripcion ESPAÑOL",
  APP_PRICING_CARD_2_BUTTON: "TEXTO DEL BOTON",

  APP_PRICING_CARD_3_TITLE: "Medium Company",
  APP_PRICING_CARD_3_ICON: "business",
  APP_PRICING_CARD_3_PRICE: "€€",
  APP_PRICING_CARD_3_BODY: "EMPRESA_MEDIANA descripcion ESPAÑOL",
  APP_PRICING_CARD_3_BUTTON: "TEXTO DEL BOTON",

  APP_PRICING_CARD_4_TITLE: "CUSTOM",
  APP_PRICING_CARD_4_ICON: "auto_fix_high",
  APP_PRICING_CARD_4_PRICE: "Contact with us",
  APP_PRICING_CARD_4_BODY: "CUSTOM descripcion ESPAÑOL",
  APP_PRICING_CARD_4_BUTTON: "TEXTO DEL BOTON",

  ////////////////////////////////////////////////////////

  APP_GRAPH_CLOUDS_LEGEND_SENSIBLE: "Sensible",
  APP_GRAPH_CLOUDS_LEGEND_LEAKAGES: "Leakage",

  APP_GRAPH_EQUALIZER_MULTIPLE_LEGEND_SENSIBLE: "Sensible",
  APP_GRAPH_EQUALIZER_MULTIPLE_LEGEND_LEAKAGES: "Leakage",

  APP_GRAPH_MAPH_TITLE: "Location",

  APP_GRAPH_CHEDDAR_LEGEND_SENSIBLE: "Sensible",
  APP_GRAPH_CHEDDAR_LEGEND_LEAKAGES: "Leakage",

  APP_DAHSBOARD_NOTIFICATION_TEXT_NO_ACTIVITY: "You still have no activity!",
  APP_DAHSBOARD_NOTIFICATION_TEXT_ADD_COMPANIES:
    "Add companies to get started.",

  APP_S_INFO_TABLE_TITLE: "My companies",
  APP_S_INFO_TABLE_FILTER: "Filter by state",
  APP_S_INFO_TABLE_DOWNLOAD_INFO: "Complete information for download.",
  APP_S_INFO_TABLE_COLUMN_TITLE_WEB: "Web",
  APP_S_INFO_TABLE_COLUMN_TITLE_COMPANY: "Company",
  APP_S_INFO_TABLE_COLUMN_TITLE_DOMAIN: "Domain",
  APP_S_INFO_TABLE_COLUMN_TITLE_APLICATION_DATE: "Request",
  APP_S_INFO_TABLE_COLUMN_TITLE_STATE: "State",
  APP_S_INFO_TABLE_COLUMN_TITLE_METADATA: "Metadata",
  APP_S_INFO_TABLE_COLUMN_TITLE_RGPD: "RGPD",

  APP_S_INFO_TABLE_STATES_ANALYZE: "Analyze",
  APP_S_INFO_TABLE_STATES_FINISHED: "Finished",
  APP_S_INFO_TABLE_STATES_ANALYZING: "Analyzing",
  APP_S_INFO_TABLE_STATES_WORKING: "Working",
  APP_S_INFO_TABLE_STATES_QUEUED: "In queue",
  APP_S_INFO_TABLE_STATES_WITHOUT_ANALYZING: "Not analyzed",

  APP_S_INFO_TABLE_STATES_FILTER_ALL: "All",
  APP_S_INFO_ACTIONS_VIEW_REPORT: "View report",
  APP_S_INFO_ACTIONS_CLEANING: "Clean metadata",
  APP_S_INFO_ACTIONS_RGPD_ANALYSE: "Check",
  APP_S_INFO_ACTIONS_CLEANING_DOWNLOAD_ZIP: "Download clean files",

  APP_S_INFO_ACTIONS_CLEANING_TITLE_MODAL:
    "Do you want to clean the domain metadata:",
  APP_S_INFO_ACTIONS_CLEANING_TEXT_MODAL: "This may involve additional costs",
  APP_S_INFO_ACTIONS_CLEANING_TITLE_YES: "Yes, clean it",

  APP_S_INFO_CLEANING_ENDED_TITLE: "Expired on: ",
  APP_S_INFO_CLEANING_AVAILABLE_UNTIL_TITLE: "Available until: ",
  APP_S_INFO_CLEANING_TOO_MANY_FILES_TITLE: "Contact: sos@suments.com",

  APP_S_INFO_TABLE_REPORT_LEAKAGES_NUM_OF_APPEARANCES: "No. of Appearances",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_DATA: "Data",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_BAN: "Ban",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_BAN_TEXT: "Hide this data from my table",
  APP_S_INFO_TABLE_REPORT_LEAKAGES_DIRECT_SEARCH: "Direct search",

  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_TITLE: "Are you sure you want to ban",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_ALERT:
    "This action cannot be undone!",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_YES:
    "Yes, nothing would make me happier",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_NO: "No",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_ERRASING:
    "We are deleting all appearances",

  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_FINISHED: "Finished!",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_DISCARD_ELIMINATED: "We have eliminated",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_LEAKS_WITH_DATA: "leaks with the data",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_SOMETHING_WENT_WRONG:
    "Oops! Something went wrong",
  APP_REPORT_S_LEAKAGES_TABLE_BAN_TRY_LATER: "Try again later",

  APP_S_DOCUMENTS_TABLE_COLUMN_FACT: "Data",
  APP_S_DOCUMENTS_TABLE_COLUMN_DOCUMENT: "Document",
  APP_S_DOCUMENTS_TABLE_COLUMN_DOMAIN: "Domain",
  APP_S_DOCUMENTS_TABLE_COLUMN_FILE_TYPE: "File type",

  APP_S_DOMAINS_TABLE_COLUMN_FACT: "Data",
  APP_S_DOMAINS_TABLE_COLUMN_DOMAIN: "Domain",
  APP_S_DOMAINS_TABLE_COLUMN_RISKS: "Risks",

  APP_REPORT_S_RAW_TABLE_COLUMN_SEVERITY: "Seepage severity",
  APP_REPORT_S_RAW_TABLE_COLUMN_DATA: "Filtered data",
  APP_REPORT_S_RAW_TABLE_COLUMN_DOMAIN: "Domain",
  APP_REPORT_S_RAW_TABLE_COLUMN_LINK: "Link to file",
  APP_REPORT_S_RAW_TABLE_COLUMN_FILE_TYPE: "File type",

  /////////////////////////////////////////////////////////////
  APP_REPORT_S_SUMMARY_BUTON_COPY_URL: "Copy url of the report",
  APP_REPORT_S_SUMMARY_BUTON_COPY_URL_HIDE: "Anonymized report",
  APP_REPORT_S_SUMMARY_ANALYSIS_TITLE: "Metadata analysis",
  APP_REPORT_S_SUMMARY_TITLE: "Summary",
  APP_REPORT_S_REPORT: "Leaks found",
  APP_REPORT_S_SUMMARY_LEAKSFOUND_TITLE: "Leaks found",
  APP_REPORT_S_SUMMARY_LEAKSNOTFOUND_TITLE:
    "No leaks have been found for this domain",
  APP_REPORT_S_SUMMARY_LEAKSFOUND_SUBTITLE: "Data leaks have been found",
  APP_REPORT_S_SUMMARY_LEAKSFOUND_BUTTON: "See leaks",
  APP_REPORT_S_SUMMARY_DOCUMENTS_TITLE: "Documents analyzed",
  APP_REPORT_S_SUMMARY_DOCUMENTS_SUBTITLE: "Analyzed with Verics",
  APP_REPORT_S_SUMMARY_METAWASH_TITLE: "Metawash",
  APP_REPORT_S_SUMMARY_METAWASH_SUBTITLE: "Clean up leaks found with Metawash",
  APP_REPORT_S_SUMMARY_METAWASH_BUTTON: "Clean leaks",
  APP_REPORT_S_SUMMARY_METAWASH_DOWNLOAD: "Download files",
  APP_REPORT_S_SUMMARY_METAWASH_TOO_MANY_FILES: "Too many files",
  APP_REPORT_S_SUMMARY_METAWASH_AVAILABLE_TITLE: "Available until:",
  APP_REPORT_S_SUMMARY_METAWASH_STATE_TITLE: "Metawash status:",
  APP_REPORT_S_SUMMARY_METAWASH_EXPIRED_TITLE: "Expired on:",

  APP_REPORT_S_SUMMARY_RGPD_TITLE: "GDPR Compliance",
  APP_REPORT_S_SUMMARY_RGPD_SUBTITLE:
    "Check if the website complies with data protection regulations.",
  APP_REPORT_S_SUMMARY_RGPD_BUTTON: "GDPR Compliance",
  APP_REPORT_S_REPORT_TITLE: "Full report",
  APP_REPORT_S_LEAKAGES_TITLE: "Potential leakages",
  APP_REPORT_S_LEAKAGES_SUBTITLE:
    "This section shows all the leaks found for the analyzed domain. You can get an overview of the volume of leaks and each data found.",
  APP_REPORT_S_SUBTITLE_SEE_MORE: "Learn more",
  APP_REPORT_S_LEAKAGES_TABLE_SUBTITLE:
    "Here you can see in detail the data found, the number of appearances and make a quick search in the networks.",
  APP_REPORT_S_LEAKAGES_EQUALIZER_TITLE: "Potential leaks",
  APP_REPORT_S_LEAKAGES_CHEDDAR_TITLE: "Potential leaks",
  APP_REPORT_S_LEAKAGES_TABLE_FILTER_TITLE: "Filter by severity",
  APP_REPORT_S_LEAKAGES_TABLE_FILTER_OPTION_ALL: "All",

  APP_REPORT_S_DOCUMENTS_TITLE: "Documents",
  APP_REPORT_S_DOCUMENTS_SUBTITLE:
    "Here you can see the leaks we have found according to the type of file in which they are found. This way you can identify the most affected leakage channels.",
  APP_REPORT_S_DOCUMENTS_TABLE_SUBTITLE:
    "In this table you can see in which file each filtration is located. You can access the file in one click.",
  APP_REPORT_S_DOCUMENTS_EQUALIZER_TITLE:
    "This is how your data leakage is distributed",
  APP_REPORT_S_DOCUMENTS_CLOUDS_TITLE: "File type distribution",
  APP_REPORT_S_DOCUMENTS_TABLE_FILTER_TITLE: "Filter by document type",

  APP_REPORT_S_DOMAINS_TITLE: "Domain and subdomains",
  APP_REPORT_S_DOMAINS_SUBTITLE:
    "Sometimes domains have subdomains. Here you can see how the leaks are distributed according to the different subdomains found. You can also take a look at the geographical distribution of the servers on which they are located.",
  APP_REPORT_S_DOMAINS_TABLE_SUBTITLE:
    "Here you can clearly see in which subdomain each filtration is located.",
  APP_REPORT_S_DOMAINS_EQUALIZER_TITLE:
    "Here is the distribution of leaks grouped by domains",
  APP_REPORT_S_DOMAINS_MAP_TITLE: "Location of the domains found",
  APP_REPORT_S_DOMAINS_TABLE_FILTER_TITLE: "Filter by severity",

  BF_FOOTER_DEVELOP_TITLE: "Technology developed by",

  /////////////////////////////////////////////////////////////////
  APP_ADD_DOMAIN_FORM_TITLE: "Add company",
  APP_ADD_DOMAIN_FORM_SUBTITLE: "Enter your data here",
  APP_ADD_DOMAIN_FORM_COMPANY_NAME: "*Company name",
  APP_ADD_DOMAIN_FORM_COMPANY_CONTACT_NAME: "Company contact",
  APP_ADD_DOMAIN_FORM_COMPANY_EMAIL: "Contact Email",
  APP_ADD_DOMAIN_FORM_COMPANY_TELEPHONE: "Contact telephone number",
  APP_ADD_DOMAIN_FORM_COMPANY_WEBSITE: "*Web (e.g. www.suments.com)",

  APP_COPY_NOTIFICATION_MESSAGE: "Link copied to clipboard",

  APP_RGPD_POPUP_TEXT:
    "We are developing this new feature, if you are interested please contact us at ",
  APP_RGPD_POPUP_TITLE: "GDPR Compliance ",

  //////////////////////////////////////////////////////////////
  APP_ADD_COMPANY_ERROR_NAME_EMPTY: "Please enter the company name",
  APP_ADD_COMPANY_ERROR_DOMAIN_EMPTY: "Please enter the domain to be analyzed",
  APP_ADD_COMPANY_ERROR_INVALID_EMPTY:
    "This domain name is not valid. You can see examples of valid domain names ",
  APP_ADD_COMPANY_ERROR_INVALID_EMPTY_LINK: "here.",
  APP_ADD_COMPANY_BUTTON: "Add company",
  APP_BF_ADD_DOMAIN_TEXT_COPIED: "Text copied to clipboard",
  APP_BF_ADD_DOMAIN_ERROR_COPY_TEXT: "Text could not be copied to clipboard"
};
