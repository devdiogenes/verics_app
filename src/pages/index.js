import EditProfileForm from "@/components";
import UserCard from "@/components";

import ContactForm from "@/components";

// Graphs
import SGraphSnake from "./Graphs/SGraphSnake.vue";
import SGraphMap from "./Graphs/SGraphMap.vue";
import SGraphCheddar from "./Graphs/SGraphCheddar.vue";
import SGraphEqualizer from "./Graphs/SGraphEqualizer.vue";
import SGraphClouds from "./Graphs/SGraphClouds.vue";

import AsyncWorldMap from "./WorldMap/AsyncWorldMap.vue";

// Tables
import SInfoTable from "./Tables/SInfoTable.vue";
import SInfoModal from "./Modals/SInfoModal.vue";
import SinfoProductCardTable from "./Tables/SinfoProductCardTable.vue";
import SLocationsTable from "./Tables/SLocationsTable.vue";
import GlobalSalesTable from "./Tables/GlobalSalesTable.vue";

export {
  EditProfileForm,
  UserCard,
  ContactForm,
  SGraphSnake,
  SGraphMap,
  SGraphCheddar,
  SGraphEqualizer,
  SGraphClouds,
  AsyncWorldMap,
  SInfoTable,
  SinfoProductCardTable,
  SInfoModal,
  SLocationsTable,
  GlobalSalesTable
};
